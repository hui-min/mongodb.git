<?php

namespace hamster\mongodb;


class DbManager
{
	/**
	 * 数据库连接实例
	 * @var array
	 */
	protected static $instance = null;

	/**
	 * 数据库配置
	 * @var array
	 */
	protected $config = [];

	/**
	 * Model constructor.
	 * @param $config['username','password','host','port','database']
	 */
	public function __construct($config)
	{
		$this->config = $config;

		Model::setConnection($config);
	}

	public static function getInstance($config)
	{
		if (!self::$instance instanceof self) {
			self::$instance = new self($config);
		}

		return self::$instance;
	}

}